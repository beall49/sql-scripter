﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;
//https://github.com/libgit2/libgit2sharp/wiki/git-branch


namespace SQLScripter {
    class GitInterface {
        string ALL = "*", 
               path, 
               branch,
               MSG,
               USER,
               PWD;

        /* set signor to automated values */
        Signature author = new Signature("Cmd Line", "@system", DateTime.Now);

        public GitInterface(Repo repo) {
            var settings = Properties.Settings.Default;
            path = repo.path;
            branch = repo.branch;
            MSG = settings.MSG;
            USER = settings.GIT_USER;
            PWD = settings.GIT_PWD;
        }

        /* 
            attemps to get the correct repo 
            based off passed in object 
        */
        public void getCorrectBranch() {
            Console.WriteLine("Grabbin the repo");
            using (var _repo = new Repository(path)) {
                foreach (Branch branch in _repo.Branches.Where(b => b.FriendlyName.Equals(branch))) {
                    Commands.Checkout(_repo, branch);
                }
            }

        }

        /* adds all objects to repo */
        public void addAll() {
            Console.WriteLine("Adding all the things");
            using (var _repo = new Repository(path)) {
                Commands.Stage(_repo, this.ALL);
            }
        }

        /* commiting to repo */
        public void commitChanges() {
            Console.WriteLine("Commiting the changes");
            using (var _repo = new Repository(path)) {
                _repo.Commit(MSG + " " + DateTime.Now.ToString(), author, author);
            }
        }

        /* pushing to remote */
        public void pushToRemote() {
            Console.WriteLine("Push it...push it real good...");
            using (var repo = new Repository(path)) {
                PushOptions options = new PushOptions();
                options.CredentialsProvider = new CredentialsHandler(
                    (url, usernameFromUrl, types) =>
                        new UsernamePasswordCredentials() {
                            Username = decodeString(USER),
                            Password = decodeString(PWD)
                        });
                repo.Network.Push(repo.Branches[branch], options);
            }
        }

        public string decodeString(string b64String) {
            return Encoding.UTF8.GetString(Convert.FromBase64String(b64String));
        }
    }
}
