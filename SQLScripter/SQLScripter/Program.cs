﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Diagnostics;
//https://github.com/libgit2/libgit2sharp/wiki/git-branch

namespace SQLScripter {
    class Program {
        public const string DEV = "dev", 
                            API = "api", 
                            ACC = "acc", 
                            PRD = "prd", 
                            MASTER = "master";

        static void Main(string[] args) {           
            Console.WriteLine("Please enter a branch, default is acc");

            /*
                read user branch input
            */
            var input = Console.ReadLine();
            Repo repo = getCorrectBranch(input);

            /* 
                if unable to find the repo, 
                default to ACC 
            */
            if (String.IsNullOrEmpty(repo.server)) {
                /* default to ACC */
                Console.WriteLine("Im setting this to ACC - Y/N?");
                input = Console.ReadLine();
                /* let the user out if != Y */
                if (input == "Y") {
                    repo = getCorrectBranch(ACC);
                } else {
                    Console.WriteLine("Fine, whatever, do it right next time then.\nHit Enter, jerk");
                    Console.ReadLine();
                    return;
                }                
            } else {
                Console.WriteLine(string.Format("Im setting the branch too {0}", repo.branch));
            }

            /* 
                creates new scripter object 
                and attemps to connect 
            */
            Scripter scripter = new Scripter(repo);
            if (scripter.CreateConnection()) {
                Console.WriteLine("wubba lubba dub dub!");
                /* we have connected, now get objects */
                scripter.Main();

                /* we've gotten all the objects, upload to git */
                GitInterface git = new GitInterface(repo);
                git.getCorrectBranch();
                git.addAll();
                git.commitChanges();
                git.pushToRemote();
                Console.WriteLine("I pushed to the remotes\nYou're welcome...");
            }
        }

        /* attemps to find the correct branch */
        public static Repo getCorrectBranch(string branch) {
            /* all repo settings are stored in Settings.settings */
            var settings = Properties.Settings.Default;
            Repo repo = new Repo();
            repo.branch = branch.ToLower();
            repo.user = settings.USER;
            repo.tbl = settings.MES_TBL;
            repo.db = settings.DB;

            /* try and find the correct repo */
            switch (branch.ToLower()) {
                case DEV:
                    repo.path = settings.DEV_PATH;
                    repo.server = settings.DEV_SRV;
                    repo.pwd = settings.DEV_PWD;                                      
                    return repo;
                case ACC:
                    repo.path = settings.ACC_PATH;
                    repo.server = settings.ACC_SRV;
                    repo.pwd = settings.ACC_PWD;
                    return repo;
                case API:
                    repo.branch = settings.API_BRANCH;
                    repo.path = settings.API_PATH;
                    repo.server = settings.API_SRV;
                    repo.db = settings.API_DB;
                    repo.user = settings.API_USR;
                    repo.pwd = settings.API_PWD;
                    repo.tbl = settings.API_TBL;
                    return repo;
                case PRD: case MASTER:
                    repo.branch = settings.PRD_BRANCH;
                    repo.path = settings.PRD_PATH;
                    repo.server = settings.PRD_SRV;
                    repo.pwd = settings.PRD_PWD;
                    return repo;
                default:
                    return repo;
            }
            
        }
    }

    /* Repository struct */
    struct Repo {
        public string db { get; set; }
        public string path { get; set; }
        public string branch { get; set; }
        public string server { get; set; }
        public string user { get; set; }
        public string pwd { get; set; }
        public string tbl { get; set; }
    }
}
