﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SQLScripter.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.0.1.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("moddbs066")]
        public string PRD_SRV {
            get {
                return ((string)(this["PRD_SRV"]));
            }
            set {
                this["PRD_SRV"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("PWD")]
        public string PRD_PWD {
            get {
                return ((string)(this["PRD_PWD"]));
            }
            set {
                this["PRD_PWD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("g3mesacc")]
        public string ACC_SRV {
            get {
                return ((string)(this["ACC_SRV"]));
            }
            set {
                this["ACC_SRV"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ignition")]
        public string USER {
            get {
                return ((string)(this["USER"]));
            }
            set {
                this["USER"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("PWD")]
        public string ACC_PWD {
            get {
                return ((string)(this["ACC_PWD"]));
            }
            set {
                this["ACC_PWD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("g3mesdev")]
        public string DEV_SRV {
            get {
                return ((string)(this["DEV_SRV"]));
            }
            set {
                this["DEV_SRV"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("PWD")]
        public string DEV_PWD {
            get {
                return ((string)(this["DEV_PWD"]));
            }
            set {
                this["DEV_PWD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("mes_runtime")]
        public string DB {
            get {
                return ((string)(this["DB"]));
            }
            set {
                this["DB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\g3mesacc\\d$\\utils\\Scripts\\dev")]
        public string DEV_PATH {
            get {
                return ((string)(this["DEV_PATH"]));
            }
            set {
                this["DEV_PATH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\g3mesacc\\d$\\utils\\Scripts\\acc")]
        public string ACC_PATH {
            get {
                return ((string)(this["ACC_PATH"]));
            }
            set {
                this["ACC_PATH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\g3mesacc\\d$\\utils\\Scripts\\prd")]
        public string PRD_PATH {
            get {
                return ((string)(this["PRD_PATH"]));
            }
            set {
                this["PRD_PATH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Automated Commit")]
        public string MSG {
            get {
                return ((string)(this["MSG"]));
            }
            set {
                this["MSG"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("master")]
        public string PRD_BRANCH {
            get {
                return ((string)(this["PRD_BRANCH"]));
            }
            set {
                this["PRD_BRANCH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("asdfas")]
        public string GIT_USER {
            get {
                return ((string)(this["GIT_USER"]));
            }
            set {
                this["GIT_USER"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("PWD")]
        public string GIT_PWD {
            get {
                return ((string)(this["GIT_PWD"]));
            }
            set {
                this["GIT_PWD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("moddbs066")]
        public string API_SRV {
            get {
                return ((string)(this["API_SRV"]));
            }
            set {
                this["API_SRV"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api_logging")]
        public string API_DB {
            get {
                return ((string)(this["API_DB"]));
            }
            set {
                this["API_DB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("S-G3INTEGRATION")]
        public string API_USR {
            get {
                return ((string)(this["API_USR"]));
            }
            set {
                this["API_USR"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("PWD")]
        public string API_PWD {
            get {
                return ((string)(this["API_PWD"]));
            }
            set {
                this["API_PWD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\g3mesacc\\d$\\utils\\Scripts\\api")]
        public string API_PATH {
            get {
                return ((string)(this["API_PATH"]));
            }
            set {
                this["API_PATH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api")]
        public string API_BRANCH {
            get {
                return ((string)(this["API_BRANCH"]));
            }
            set {
                this["API_BRANCH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("tbl")]
        public string API_TBL {
            get {
                return ((string)(this["API_TBL"]));
            }
            set {
                this["API_TBL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("G3")]
        public string MES_TBL {
            get {
                return ((string)(this["MES_TBL"]));
            }
            set {
                this["MES_TBL"] = value;
            }
        }
    }
}
